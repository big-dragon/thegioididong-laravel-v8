<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'quantity', 'description', 'slug', 'price', 'category_id', 'brand_id', 'storage_id'];

    // Relationships
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function storage()
    {
        return $this->belongsTo(Storage::class);
    }
    public  function productDetails()
    {
        return $this->hasMany(ProductDetail::class);
    }
    public function colors()
    {
        return $this->belongsToMany(Color::class, 'product_details', 'product_id', 'color_id');
    }
}
