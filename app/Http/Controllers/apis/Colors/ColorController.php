<?php

namespace App\Http\Controllers\apis\Colors;

use App\Http\Controllers\Controller;
use App\Models\Color;

class ColorController extends Controller
{
    public function index()
    {
        return response()->json(Color::all(), 200);
    }
}
