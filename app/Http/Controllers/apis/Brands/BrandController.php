<?php

namespace App\Http\Controllers\apis\Brands;

use App\Http\Controllers\Controller;
use App\Models\Brand;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::select('id', 'name')->get();
        return response()->json($brands, 200);
    }
}
