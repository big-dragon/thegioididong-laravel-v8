<?php

namespace App\Http\Controllers\apis\Orders;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Products\ProductController;
use App\Http\Controllers\Vouchers\VoucherController;
use App\Http\Requests\Api\Orders\CreateOrderRequest;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->hasAny(['status', 'userInfo'])) {
            $data = DB::table('orders')
                ->where('status', $request->input('status'))
                ->orWhere('name', 'like', $request->query('userInfo'))
                ->orWhere('phone', 'like', $request->query('userInfo'))
                ->select('orders.*')
                ->get();
	    return response()->json($data, 200);
        } else {
            return response()->json(Order::orderBy('id', 'desc')->get(), 200);
        }
    }

    /*
     * Get all order where user id = id?
    **/
    public function getOrders($id)
    {
        try {
            return response()->json(Order::where('user_id', $id)->orderBy('id', 'desc')->paginate(1), 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function getOrderDetails($id)
    {
        try {
            $orders = Order::where('id', $id)->first();
            $orderDetails = OrderDetail::with('product.images')->where('order_id', $id)->get();
            $voucherDetails = VoucherDetail::with('voucher')->where('order_id', $id)->first();
            $data = [
                'orders' => $orders,
                'orderDetails' => $orderDetails,
                'voucherDetails' => $voucherDetails
            ];
            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateOrderRequest $request)
    {
        $orderInformation = $request->only(['name', 'user_id', 'phone', 'address', 'total_price']);
//        return response()->json($orderInformation);
//        dd($orderInformation);
        $voucherCode = $request->input('code'); // Get voucher code
        $cartItems = $request->input('cartItems');
        $arr = []; // Initialization array to collect all cart's item

        foreach ($cartItems as $value) {
            $arr[] = json_decode($value, true);
        }
//        dd($arr[0]);
        DB::beginTransaction();

        try {
            $order = Order::create($orderInformation);

            $order->orderDetails()->createMany($arr[0]); // store cart to order detail table

            if (!is_null($voucherCode) && VoucherController::checkInvalidVoucher($voucherCode) === false) {
                $voucher = VoucherController::getCurrentVoucher($voucherCode);

                if (VoucherController::checkMinPriceToApplyVoucher($voucher, $orderInformation['total_price']) === true) {
                    $voucherDetail = [
                        'voucher_id' => $voucher->id,
                        'used_at' => now()
                    ];
                    $discount = $voucher->discount;

                    $totalPrice = VoucherController::applyDiscount($orderInformation['total_price'], $discount);

                    Order::where('id', $order->id)->update(['total_price' => $totalPrice]);


                    VoucherController::updateVoucherQuantityAfterOrder($voucher); // update voucher quantity after user confirm order

                    $order->voucherDetails()->create($voucherDetail);
                }
            }

            ProductController::updateProductQuantityAfterOrder($arr[0]); // update product quantity after user confirm order

            DB::commit();

            return response()->json('success', 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
