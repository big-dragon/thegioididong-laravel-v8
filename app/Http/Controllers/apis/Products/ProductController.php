<?php

namespace App\Http\Controllers\apis\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Product\CreateProductRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        return response()->json(Product::with('images')->paginate(10), 200); // return 10 products/page
    }

    public function store(CreateProductRequest $request)
    {
        DB::beginTransaction();

        try {
            $arr = [];
            foreach ($request->input('productColors') as $value) {
                $arr[] = json_decode($value, true);
            }

            $fields = $request->only(['name', 'price', 'description', 'brand_id', 'category_id', 'storage_id']); // Product's fields
            $slug = Str::slug($request->input('name'), '-') . time(); // Product's slug name
            $fields['slug'] = $slug;
            $target_dir = public_path('storage/images/products/'); // Target directory for saving images
            $images = [];

            $product = Product::create($fields); // Create new product
            $product->productDetails()->createMany($arr[0]); // Create product detail
            foreach ($request->file('files') as $key => $file) {
                $fileName = time() . $file->getClientOriginalName(); // Set file name
                $file->move($target_dir, $fileName); // Move file to target dir

                $images[$key]['product_id'] = $product->id;
                $images[$key]['path'] = $fileName;
                $images[$key]['created_at'] = now();
                $images[$key]['updated_at'] = now();
            }

            $product->images()->createMany($images);

            DB::commit();

            return response()->json('Create ok', 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        if (Product::where('id', $id)->exists()) { // If product is exists when return product
            $product = Product::with(['images','colors.productDetails','brand', 'category', 'storage'])->where('id', $id)->first();
            return response()->json(['product' => $product, 200]);
        } else {
            return response()->json(['message' => 'Product not found'], 404);
        }
    }
}
