<?php

namespace App\Http\Controllers\apis\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;

class UserController extends Controller
{
    public function getInformation($userId)
    {
        if (Profile::where('user_id', $userId)->exists()) { // If product is exists when return profile
            $profile = Profile::with('user')->where('user_id', $userId)->get();
            return response()->json(['profile' => $profile, 200]);
        } else {
            return response()->json(['message' => 'Profile user not found'], 404);
        }
    }
}
