<?php

namespace App\Http\Controllers\apis\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category;


class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::select('id', 'name')->get();
        return response()->json($categories, 200);
    }
}
