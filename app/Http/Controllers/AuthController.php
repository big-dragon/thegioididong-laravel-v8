<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register(\App\Http\Requests\Auth\RegisterRequest $request)
    {
        DB::beginTransaction();

        try {
            $fields = $request->only(['name', 'email', 'password']);
            $fields['password'] = bcrypt($fields['password']);

            $userProfile = $request->only(['phone', 'address']);

            $user = User::create($fields);
            $user->profile()->create($userProfile);

            DB::commit();

            $message = 'Register successfully';
            return response()->json(['success' => $message], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        return response($message);
    }

    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json('Email or password is not correct!', 401);
        }
        $token = $user->createToken('myToken')->plainTextToken;
        $response = [
            'token' => $token,
            'user' => $user
        ];
        return response()->json($response, 201);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'Logged out'
        ]);
    }
}
