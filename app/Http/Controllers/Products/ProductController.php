<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\ProductDetail;

class ProductController extends Controller
{
    /**
     * @param $items -- List item products in order
     * @return void
     */
    public static function updateProductQuantityAfterOrder($items)
    {
        foreach ($items as $value) {
            $productDetail = ProductDetail::where('product_id', $value['product_id'])->where('color_id', $value['color_id'])->first();
            $quantity = $productDetail->quantity;
            $quantity -= $value['quantity'];
            ProductDetail::where('product_id', $value['product_id'])->where('color_id', $value['color_id'])->update(['quantity' => $quantity]);
        }
    }
}

