<?php

namespace App\Http\Controllers\Vouchers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Voucher;

class VoucherController extends Controller
{
    /**
     * Get current voucher with voucher's code
     *
     * @param [type] $voucherCode
     * @return voucher
     */
    public static function getCurrentVoucher($voucherCode)
    {
        return Voucher::where('code', $voucherCode)->first();
    }

    /**
     * If date start open voucher is before current date then voucher can use, otherwise can not use
     * @param [object] $voucher voucher in resources
     * @return bool true if date-start <= current time, otherwise return false
     */
    public static function checkDateStartIsBeforeCurrentDate($voucher)
    {
        if ($voucher->date_start <= now()) {
            return  true;
        }

        return false;
    }

    /**
     * Check voucher's quantity is zero
     * @param [object] $voucher voucher in resources
     * @return bool true if quantity = 0, otherwise return false
     */
    public static function checkQuantityVoucherIsZero($voucher)
    {
        if ($voucher->quantity == 0) {
            return true;
        }

        return false;
    }

    /**
     * Check min price of order to apply voucher, if min_price <= total price then apply voucher code
     * @param  [object] $voucher voucher in resources
     * @param int $totalPrice total price of order
     */
    public static function checkMinPriceToApplyVoucher($voucher, $totalPrice)
    {
        if ($voucher->min_price <= $totalPrice) {
            return true;
        }
        return false;
    }

    /**
     * Return true if voucher' is_active column is true
     *
     * @param [string] $voucherCode
     * @return bool true if voucher unexpired
     */
    public static function checkIsActiveVoucher($voucher)
    {
        if ($voucher->is_active === true) {
            return true;
        }
    }

    /**
     * Check invalid voucher if voucher not exist in resources
     *
     * @param [string] $voucherCode
     * @return bool true if voucher is invalid, otherwise return false
     */
    public static function checkInvalidVoucher($voucherCode)
    {
        $voucher = self::getCurrentVoucher($voucherCode);
        if (is_null($voucher)) { // if not exist voucher
            return true;
        }

        if (self::checkIsActiveVoucher($voucher) && self::checkDateStartIsBeforeCurrentDate($voucher) === true && self::checkQuantityVoucherIsZero($voucher) === false) {
            return false;
        } else{
            return true;
        }
    }

    /**
     * @param $totalPrice -- the total price of order
     * @param $discount -- discount of voucher
     * @return float|int
     */
    public static function applyDiscount($totalPrice, $discount)
    {
        $discountPrice = ($totalPrice * $discount) / 100;
        return $totalPrice -= $discountPrice;
    }

    public static function updateVoucherQuantityAfterOrder($voucher)
    {
        $quantity = $voucher->quantity - 1;
        return Voucher::where('id', $voucher->id)->update(["quantity" => $quantity]);
    }
}
