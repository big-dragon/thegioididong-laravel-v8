<?php

namespace App\Http\Requests\Api\Admins\Voucher;

use Illuminate\Foundation\Http\FormRequest;

class CreateVoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'code' => 'required|string|min:5|max:25',
            'date_start' => 'required|date',
            'date_end' => 'required|date|after_or_equal:date_start',
            'discount' => 'required|numeric|min:1|max:50',
            'min_price' => 'required|min:1|',
            'quantity' => 'required|min:1',
        ];
    }
}
