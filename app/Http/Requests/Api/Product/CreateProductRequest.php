<?php

namespace App\Http\Requests\Api\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|min:1|numeric',
//            'quantity' => 'required|min:1|numeric',
            'description' => 'required',
            'brand_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'productColors' => 'required',
            'files' => 'required|array|min:1|max:7',
            'files.*' => 'image'
        ];
    }
}
