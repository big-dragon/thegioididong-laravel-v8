<?php

namespace App\Console\Commands;

use App\Models\Voucher;
use Illuminate\Console\Command;

class CheckIsActiveVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:voucher_is_active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if date_end of voucher is before current date then change status is_active to false';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Voucher::whereDate('date_end', '<', now())
            ->orWhere('quantity', 0)
            ->update(['is_active' => false]);

        $this->info('A Server now is checking voucher status');
    }
}
