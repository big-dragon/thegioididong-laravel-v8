<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create(['name' => 'admin', 'email' => 'phuoc04012000@gmail.com', 'password' => bcrypt('ngocphuocha'), 'role_id' => Role::SUPER_ADMIN_ROLE]);
        $admin->profile()->create(['phone' => '0984641362', 'address' => 'Hoi An']);

        $user = User::create(['name' => 'phuoctn', 'email' => 'phuoctn412@gmail.com', 'password' => bcrypt('ngocphuocha'), 'role_id' => Role::NORMAL_USER_ROLE]);
        $user->profile()->create(['phone' => '0984642362', 'address' => 'Hoi An']);
    }
}
