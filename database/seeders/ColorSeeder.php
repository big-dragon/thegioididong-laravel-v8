<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::insert([
            ['color' => 'Đỏ'],
            ['color' => 'Xanh'],
            ['color' => 'Vàng'],
            ['color' => 'Đen'],
            ['color' => 'Xám']
        ]);
    }
}
