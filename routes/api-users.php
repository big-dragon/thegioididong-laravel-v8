<?php

use App\Http\Controllers\apis\Users\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'throttle:global',]], function () {
    Route::get('/users/{id}/profiles', [UserController::class, 'getInformation']);
});
