<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apis\Brands\BrandController;
use App\Http\Controllers\apis\Categories\CategoryController;
use App\Http\Controllers\apis\Products\ProductController;
use App\Http\Controllers\apis\Comments\CommentController;

Route::group(['middleware' => ['throttle:global']], function () {
    Route::get('/brands', [BrandController::class, 'index']);
    Route::get('/categories', [CategoryController::class, 'index']);
    Route::get('/products', [ProductController::class, 'index']);
    Route::get('/products/{id}', [ProductController::class, 'show']);
    Route::get('/products/{id}/comments', [CommentController::class, 'getCommentByProduct']);
    Route::get('/storages', [\App\Http\Controllers\apis\Storages\StorageController::class, 'index']); // get all storage
    Route::get('/colors', [\App\Http\Controllers\apis\Colors\ColorController::class, 'index']);
});
